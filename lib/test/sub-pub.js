/*jslint nomen:true, node: true, white:true */
/*globals describe, it */
"use strict";
var assert = require("assert");
var observer = require("../../index");

describe("simple subscription publish test", function () {
    it("key: hello arg: world", function (done) {
        var instance; 
        
        function next(arg){
            assert(arg === "world", "should be the same as world");
            instance.off();
            done();
        }
        
        instance = observer.on("hello", next);
        //allows shows any errors when sending a message
        observer.send("hellow", "world", true);
        observer.send("hello", "world");
    });
});
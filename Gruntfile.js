/*jslint nomen:true, node: true, white:true */
/*globals */
"use strict";

module.exports = function (grunt) {
    grunt.initConfig({
        pkg: require("./package"),
        mochaTest: require("./lib/json/grunt-mocha")
    });

    require("./lib/json/grunt-tasks").map(function (task) {
        grunt.loadNpmTasks(task);
    });

    grunt.registerTask("default", ["mochaTest:test"]);
};
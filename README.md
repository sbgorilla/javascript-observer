# License

MIT

# Overview 

Module for any publish/subscribe handling.

# Installation

```
npm install javascript-observer --save
```

# Public Repository

https://bitbucket.org/sbgorilla/javascript-observer

# Example

```
var jo = require("javascript-observer"); 
var subcription = jo.on("message", function(o){
    console.log("arguments are " + o); 
});

jo.send("message", "some string value"); 
//prints 'arguments are some string value'
subscription.off(); 
jo.send("message", "some string value"); 
//prints 'could not send: some string value'

//NOTE: if a notification needs to be send on subscribe var subscription can also perform send
```
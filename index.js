/*jslint white:true, node:true */
/*globals console */
"use strict";

var observer = (function(){
    function Instance(o, key, next){ 
        this.o = o; 
        this.key = key; 
        this.next = next;
    }
    function remove(o, key, next){ 
        if(o && o.hasOwnProperty(key) && (o[key] instanceof Array) && (next instanceof Function)){ 
            o[key].map(function(func, i){
                if(func === next){
                    o[key].splice(i, 1);
                    return; 
                } 
            }); 
        }
    }
    function Observer(){
        this.o = {}; 
        this.on = function(key, next){
            var instance, self = this; 
            
            if(!self.o.hasOwnProperty(key)){
                self.o[key] = [];
            }
            
            if((self.o[key] instanceof Array) && (next instanceof Function)){
                self.o[key].push(next); 
            }
            
            instance = new Instance(self.o, key, next); 
            
            function onSend(key, arg, debug){ 
                self.send(key, arg, debug); 
            }
            
            function onOff(){  
                remove(instance.o, instance.key, instance.next); 
            }
            
            return{
                off: onOff, 
                send: onSend
            }; 
        };
        this.send = function(key, arg, debug){
            var self = this; 

            if(self.o.hasOwnProperty(key) && self.o[key] instanceof Array && self.o[key].length !==0){
                self.o[key].map(function(next){
                    next(arg); 
                }); 
            }
            else if(debug){
                if(typeof console === "object" && console.hasOwnProperty("log") && console.log instanceof Function){ 
                    console.log(["could not send value [",JSON.stringify(arg),"] for key","[",JSON.stringify(key),"]"].join(" ")); 
                }
            }
        }; 
    }
    return new Observer(); 
}()); 

module.exports = observer;